---------------------
Beam Position Tracking Project
---------------------
BeamPositionTracking is an application based on Tango Devices (http://www.tango-controls.org/) that will be used on beamlines to enslave the beam position on a specific target. 

![BPTPrincipe](doc/images/BPTPrincipe.png)

The back-end is made of two Tango Devices :
* [ActuatorSystem](https://gitlab.synchrotron-soleil.fr/software-control-system/tango-devices/motion/beampositiontracking/actuatorsystem) :  This device's aim is to organize movements on X and Y axis
![classesAS](doc/images/classesAS.png)


* [BeamPositionTracking](https://gitlab.synchrotron-soleil.fr/software-control-system/tango-devices/motion/beampositiontracking/beampositiontracking): This device will estimate new centroids using it's sensor. At every step it will estimate a new axis position using a PID corrector.
![classesBPT](doc/images/classesBPT.png)

Theses devices will be able to work with differents types of sub-devices for translations and several detectors such as Lima detector and XBPM using Yat4Tango::Plugin technology.

On top of those devices comes an JAVA application based on COMETE framework

![BPTCometeApp](doc/images/IMHView.png)

---------------------
![SOLEIL Synchrotron](https://www.synchrotron-soleil.fr/sites/default/files/logo_0.png)
---------------------
