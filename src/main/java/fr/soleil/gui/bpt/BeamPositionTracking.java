package fr.soleil.gui.bpt;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import fr.soleil.gui.bpt.view.ConfigPanel;

public class BeamPositionTracking {

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        ConfigPanel configPanel = new ConfigPanel();
        new Thread(configPanel, configPanel.getPreferredThreadName()).start();
        SwingUtilities.invokeLater(() -> {
            configPanel.showMainPanel(frame);
        });
    }

}
