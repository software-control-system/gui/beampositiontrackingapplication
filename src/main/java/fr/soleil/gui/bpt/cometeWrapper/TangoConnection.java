package fr.soleil.gui.bpt.cometeWrapper;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import fr.soleil.comete.box.AbstractCometeBox;
import fr.soleil.comete.box.matrixbox.NumberMatrixBox;
import fr.soleil.comete.box.scalarbox.BooleanScalarBox;
import fr.soleil.comete.box.scalarbox.NumberScalarBox;
import fr.soleil.comete.box.scalarbox.ScalarCometeBox;
import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;
import fr.soleil.data.target.ITarget;
import fr.soleil.data.target.matrix.INumberMatrixTarget;
import fr.soleil.data.target.scalar.IBooleanTarget;
import fr.soleil.data.target.scalar.INumberTarget;
import fr.soleil.data.target.scalar.IScalarTarget;
import fr.soleil.data.target.scalar.ITextTarget;

/****************************************************************
 * TangoConnection
 * 
 * To mask every COMETE connections
 **************************************************************/
public class TangoConnection {

    public static final Class<ITextTarget> STRING_TYPE = ITextTarget.class;
    public static final Class<IBooleanTarget> BOOLEAN_TYPE = IBooleanTarget.class;
    public static final Class<INumberTarget> NUMBER_TYPE = INumberTarget.class;
    public static final Class<INumberMatrixTarget> NUMBER_MATRIX_TYPE = INumberMatrixTarget.class;
    private static final Class<IScalarTarget> SCALAR_TYPE = IScalarTarget.class;

    private static final ConcurrentMap<Class<?>, AbstractCometeBox<?>> COMETE_BOX_MAP = new ConcurrentHashMap<>();

    private TangoConnection() {
    }

    /////////////////////////////////////////////////////////////////
    // connectAttribute
    //
    // For a ScalarTarget component
    /////////////////////////////////////////////////////////////////

    /**
     * Connects an {@link IScalarTarget} component to an attribute.
     * 
     * @param <T> The type of {@link IScalarTarget}.
     * @param <C> The type of component.
     * @param targetType The {@link IScalarTarget} class.
     * @param deviceName The device name.
     * @param attributeName The attribute name.
     * @param component The component.
     * @param metaData Whether to connect to meta data too.
     */
    public static <T extends IScalarTarget, C extends T> void connectAttribute(Class<T> targetType, String deviceName,
            String attributeName, T component, boolean metaData) {
        ScalarCometeBox<T, ?> box = getScalarBoxWithType(targetType);
        if (box != null) {
            TangoKey tangoKey = new TangoKey();
            TangoKeyTool.registerAttribute(tangoKey, deviceName, attributeName);
            box.connectWidget(component, tangoKey, true, metaData);
        }
    }

    /**
     * Disconnects an {@link IScalarTarget} component from an attribute
     * 
     * @param <T> The type of {@link IScalarTarget}.
     * @param <C> The type of component.
     * @param targetType The {@link IScalarTarget} class.
     * @param component The component.
     * @see #connectAttribute(Class, String, String, IScalarTarget, boolean)
     */
    public static <T extends IScalarTarget, C extends T> void disconnectAttribute(Class<T> targetType, T component) {
        disconnect(targetType, component);
    }

    /////////////////////////////////////////////////////////////////
    // connectAttribute
    //
    // For an ImageViewer component
    /////////////////////////////////////////////////////////////////

    /**
     * Connects an {@link ImageViewer} to an attribute.
     * 
     * @param deviceName The device name.
     * @param attributeName The attribute name.
     * @param component The {@link ImageViewer}.
     */
    public static void connectAttribute(String deviceName, String attributeName, ImageViewer component) {
        AbstractCometeBox<INumberMatrixTarget> boxMat = getBoxWithType(NUMBER_MATRIX_TYPE);
        TangoKey bptStatekey = new TangoKey();
        TangoKeyTool.registerAttribute(bptStatekey, deviceName, attributeName);
        boxMat.connectWidget(component, bptStatekey);
    }

    /**
     * Disconnects an {@link ImageViewer} from an attribute.
     * 
     * @param component
     * @see #connectAttribute(String, String, ImageViewer)
     */
    public static void disconnectAttribute(ImageViewer component) {
        disconnect(NUMBER_MATRIX_TYPE, component);
    }

    /////////////////////////////////////////////////////////////////
    // connectCommand
    //
    // For a ScalarTarget component
    /////////////////////////////////////////////////////////////////

    /**
     * Connects an {@link IScalarTarget} component to a command.
     * 
     * @param <T> The type of {@link IScalarTarget}.
     * @param <C> The type of component.
     * @param targetType The {@link IScalarTarget} class.
     * @param deviceName The device name.
     * @param commandName The command name.
     * @param component The component.
     * @param metaData Whether to connect to meta data too.
     */
    public static <T extends IScalarTarget, C extends T> void connectCommand(Class<T> targetType, String deviceName,
            String commandName, C component, boolean metaData) {
        ScalarCometeBox<T, ?> box = getScalarBoxWithType(targetType);
        if (box != null) {
            TangoKey tangoKey = new TangoKey();
            TangoKeyTool.registerCommand(tangoKey, deviceName, commandName);
            box.connectWidget(component, tangoKey, true, metaData);
        }
    }

    /**
     * Disconnects an {@link IScalarTarget} component from a command.
     * 
     * @param <T> The type of {@link IScalarTarget}.
     * @param <C> The type of component.
     * @param targetType The {@link IScalarTarget} class.
     * @param component The component.
     * @see #connectCommand(Class, String, String, IScalarTarget, boolean)
     */
    public static <T extends IScalarTarget, C extends T> void disconnectCommand(Class<T> targetType, C component) {
        disconnect(targetType, component);
    }

    /////////////////////////////////////////////////////////////////
    // connectProperty
    //
    // For a ScalarTarget component
    /////////////////////////////////////////////////////////////////

    /**
     * Connects an {@link IScalarTarget} component to a property.
     * 
     * @param <T> The type of {@link IScalarTarget}.
     * @param <C> The type of component.
     * @param targetType The {@link IScalarTarget} class.
     * @param deviceName The device name.
     * @param propertyName The property name.
     * @param component The component.
     */
    public static <T extends IScalarTarget, C extends T> void connectProperty(Class<T> targetType, String deviceName,
            String propertyName, C component) {
        ScalarCometeBox<T, ?> box = getScalarBoxWithType(targetType);
        if (box != null) {
            TangoKey bptStatekey = new TangoKey();
            TangoKeyTool.registerDeviceProperty(bptStatekey, deviceName, propertyName);
            box.connectWidget(component, bptStatekey);
        }
    }

    /**
     * Disconnects an {@link IScalarTarget} component from a property.
     * 
     * @param <T> The type of {@link IScalarTarget}.
     * @param <C> The type of component.
     * @param targetType The {@link IScalarTarget} class.
     * @param component The component.
     * @see #connectProperty(Class, String, String, IScalarTarget)
     */
    public static <T extends IScalarTarget, C extends T> void disconnectProperty(Class<T> targetType, C component) {
        disconnect(targetType, component);
    }

    /////////////////////////////////////////////////////////////////
    // connectDevice
    //
    // For a Label component
    /////////////////////////////////////////////////////////////////

    /**
     * Connects an {@link ITextTarget} component to a device class (so that it receives the device class).
     * 
     * @param deviceName The device name.
     * @param component The component.
     * @return Whether connection was successful.
     */
    public static boolean connectDeviceClass(String deviceName, ITextTarget component) {
        boolean connected;
        ScalarCometeBox<ITextTarget, ?> boxStr = getScalarBoxWithType(STRING_TYPE);
        TangoKey bptStatekey = new TangoKey();
        try {
            TangoKeyTool.registerDeviceClass(bptStatekey, deviceName);
            boxStr.connectWidget(component, bptStatekey);
            connected = true;
        } catch (Exception e) {
            connected = false;
        }
        return connected;
    }

    /**
     * Disconnects an {@link ITextTarget} component from a device class.
     * 
     * @param component The component.
     */
    public static void disconnectDeviceClass(ITextTarget component) {
        disconnect(STRING_TYPE, component);
    }

    /////////////////////////////////////////////////////////////////
    // AbstractCometeBox methods
    /////////////////////////////////////////////////////////////////

    private static <T extends ITarget> AbstractCometeBox<T> getBoxWithType(Class<T> targetType) {
        AbstractCometeBox<?> tmp = null;
        if (targetType != null) {
            if (NUMBER_MATRIX_TYPE.equals(targetType)) {
                NumberMatrixBox boxMat = (NumberMatrixBox) COMETE_BOX_MAP.get(NUMBER_MATRIX_TYPE);
                if (boxMat == null) {
                    boxMat = new NumberMatrixBox();
                    NumberMatrixBox former = (NumberMatrixBox) COMETE_BOX_MAP.putIfAbsent(NUMBER_MATRIX_TYPE, boxMat);
                    if (former != null) {
                        boxMat = former;
                    }
                }
                tmp = boxMat;
            } else if (SCALAR_TYPE.isAssignableFrom(targetType)) {
                tmp = getScalarBoxWithType(targetType.asSubclass(SCALAR_TYPE));
            }
        }
        @SuppressWarnings("unchecked")
        AbstractCometeBox<T> box = (AbstractCometeBox<T>) tmp;
        return box;
    }

    private static <T extends IScalarTarget> ScalarCometeBox<T, ?> getScalarBoxWithType(Class<T> targetType) {
        ScalarCometeBox<T, ?> box = null;
        if (targetType != null) {
            @SuppressWarnings("unchecked")
            ScalarCometeBox<T, ?> savedBox = (ScalarCometeBox<T, ?>) COMETE_BOX_MAP.get(targetType);
            if (savedBox == null) {
                if (STRING_TYPE.equals(targetType)) {
                    @SuppressWarnings("unchecked")
                    ScalarCometeBox<T, ?> tmp = (ScalarCometeBox<T, ?>) new StringScalarBox();
                    savedBox = tmp;
                } else if (BOOLEAN_TYPE.equals(targetType)) {
                    @SuppressWarnings("unchecked")
                    ScalarCometeBox<T, ?> tmp = (ScalarCometeBox<T, ?>) new BooleanScalarBox();
                    savedBox = tmp;
                } else if (NUMBER_TYPE.equals(targetType)) {
                    @SuppressWarnings("unchecked")
                    ScalarCometeBox<T, ?> tmp = (ScalarCometeBox<T, ?>) new NumberScalarBox();
                    savedBox = tmp;
                }
                if (savedBox != null) {
                    AbstractCometeBox<?> former = COMETE_BOX_MAP.putIfAbsent(targetType, savedBox);
                    if (former != null) {
                        @SuppressWarnings("unchecked")
                        ScalarCometeBox<T, ?> tmp = (ScalarCometeBox<T, ?>) former;
                        savedBox = tmp;
                    }
                }
            }
            box = savedBox;
        }
        return box;
    }

    private static <T extends ITarget, C extends T> void disconnect(Class<T> targetType, C component) {
        AbstractCometeBox<T> box = getBoxWithType(targetType);
        if (box != null) {
            box.disconnectWidgetFromAll(component);
        }
    }

}
