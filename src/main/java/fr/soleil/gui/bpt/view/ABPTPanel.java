package fr.soleil.gui.bpt.view;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Window;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.swing.Label;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.target.ITarget;
import fr.soleil.data.target.scalar.INumberTarget;
import fr.soleil.data.target.scalar.ITextTarget;

/**
 * A {@link JPanel}, used by BPT application, that also is a {@link Runnable}.
 * 
 * @author GIRARDOT
 */
public abstract class ABPTPanel extends JPanel implements Runnable {

    private static final long serialVersionUID = 6393225411983111117L;

    protected final Logger logger;
    protected Label asState, bptState;
    protected String bptDeviceName, xAlias, yAlias;
    protected JPanel leftPanel, rightPanel;

    public ABPTPanel(String xAlias, String yAlias, boolean initComponents, boolean initGui) {
        super(new GridLayout(0, 2));
        setAliases(xAlias, yAlias);
        logger = LoggerFactory.getLogger(getClass());
        if (initComponents) {
            initComponents();
        }
        if (initGui) {
            initGui();
        }
    }

    public void setAliases(String xAlias, String yAlias) {
        this.xAlias = xAlias;
        this.yAlias = yAlias;
    }

    /**
     * Returns the preferred name to use for a thread that would run this {@link ABPTPanel}
     * 
     * @return A {@link String}: the preferred name.
     */
    public String getPreferredThreadName() {
        return getClass().getSimpleName();
    }

    /**
     * To init components
     */
    protected void initComponents() {
        bptState = new Label();
        asState = new Label();
    }

    /**
     * To refresh interface
     */
    protected abstract void refreshInterface();

    /**
     * To init left panel.
     * 
     * @return A {@link JPanel}.
     */
    protected abstract JPanel initLeftPanel();

    /**
     * To init right panel
     * 
     * @return A {@link JPanel}
     */
    protected abstract JPanel initRightPanel();

    protected abstract Dimension getPreferredFrameSize();

    /**
     * To init user interface
     */
    protected final void initGui() {
        // Init panels
        leftPanel = initLeftPanel();
        rightPanel = initRightPanel();
        add(leftPanel);
        add(rightPanel);
    }

    protected Label generateValueLabel() {
        Label label = new Label();
        label.setCometeFont(CometeFont.DEFAULT_FONT);
        return label;
    }

    public void showMainPanel(Window mainWindow) {
        if (mainWindow instanceof Frame) {
            ((Frame) mainWindow).setTitle(getClass().getSimpleName());
        } else if (mainWindow instanceof Dialog) {
            ((Dialog) mainWindow).setTitle(getClass().getSimpleName());
        }
        if (mainWindow instanceof JFrame) {
            ((JFrame) mainWindow).setContentPane(this);
        } else if (mainWindow instanceof JDialog) {
            ((JDialog) mainWindow).setContentPane(this);
        } else {
            mainWindow.removeAll();
            mainWindow.add(this);
        }
        mainWindow.setSize(getPreferredFrameSize());
        mainWindow.setLocationRelativeTo(null);
        mainWindow.setVisible(true);
    }

    @Override
    public final void run() {
        while (Thread.currentThread().isAlive() && !Thread.currentThread().isInterrupted()) {
            try {
                Thread.sleep(100);
                // To update interface
                refreshInterface();
            } catch (InterruptedException e) {
                // nothing to do: thread is interrupted
            }
        }
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected abstract class ADataTarget implements ITextTarget {

        private volatile String data;

        public ADataTarget() {
            super();
        }

        @Override
        public void addMediator(Mediator<?> mediator) {
            // not managed
        }

        @Override
        public void removeMediator(Mediator<?> mediator) {
            // not managed
        }

        protected abstract boolean isExpectedData(String data);

        protected abstract void treatNoData();

        protected abstract void treatInvalidData();

        protected abstract void treatExpectedData();

        @Override
        public String getText() {
            return data;
        }

        @Override
        public final void setText(String deviceClass) {
            this.data = deviceClass;
            if ((deviceClass == null) || deviceClass.isEmpty()
                    || StringScalarBox.DEFAULT_ERROR_STRING.equals(deviceClass)) {
                treatNoData();
            } else if (isExpectedData(deviceClass)) {
                treatExpectedData();
            } else {
                treatInvalidData();
            }
        }

    }

    protected class Refreshment implements ITarget {

        @Override
        public void addMediator(Mediator<?> mediator) {
            // not managed
        }

        @Override
        public void removeMediator(Mediator<?> mediator) {
            // not managed
        }

        protected void refreshLater() {
            SwingUtilities.invokeLater(() -> {
                refreshInterface();
            });
        }

    }

    protected class TextRefreshment extends Refreshment implements ITextTarget {

        @Override
        public String getText() {
            // not managed
            return null;
        }

        @Override
        public void setText(String text) {
            refreshLater();
        }

    }

    protected class NumberRefreshment extends Refreshment implements INumberTarget {

        @Override
        public Number getNumberValue() {
            // not managed
            return null;
        }

        @Override
        public void setNumberValue(Number value) {
            refreshLater();
        }

    }

}
