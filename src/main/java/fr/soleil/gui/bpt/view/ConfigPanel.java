package fr.soleil.gui.bpt.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Window;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingWorker;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import fr.soleil.comete.awt.util.FontTool;
import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.swing.StringButton;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.comete.tango.data.service.helper.TangoAttributeHelper;
import fr.soleil.data.target.scalar.ITextTarget;
import fr.soleil.gui.bpt.cometeWrapper.TangoConnection;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.WindowSwingUtils;
import fr.soleil.lib.project.swing.dialog.ProgressDialog;

public class ConfigPanel extends ABPTPanel {

    private static final long serialVersionUID = 7303113494105110033L;

    private static final String BPT_DEVICE_CLASS = "BeamPositionTracking";
    private static final String X_PID_PROP = "XPIDDefinition";
    private static final String Y_PID_PROP = "YPIDDefinition";
    private static final String X_MIN_POSITION = "XAxisMinPosition";
    private static final String X_MAX_POSITION = "XAxisMaxPosition";
    private static final String Y_MIN_POSITION = "YAxisMinPosition";
    private static final String Y_MAX_POSITION = "YAxisMaxPosition";
    private static final String X_CALIBRATION_STEP_NUMBER = "CalibrationStepNbXAxis";
    private static final String Y_CALIBRATION_STEP_NUMBER = "CalibrationStepNbYAxis";
    private static final String AS_DEVICE_ADRESS = "ActuatorSystemDeviceAdress";
    private static final String COLD_X_THRESHOLD = "UI_xColdThreshold";
    private static final String HOT_X_THRESHOLD = "UI_xHotThreshold";
    private static final String COLD_Y_THRESHOLD = "UI_yColdThreshold";
    private static final String HOT_Y_THRESHOLD = "UI_yHotThreshold";
    private static final String AXES_ALIASES = "AxesAliases";

    private static final String FAVORITE_BPT_KEY = "bptFavoriteDevice";

    private static final String CONNECTING = "Connecting...";
    private static final String CONNECTING_BPT = "Connecting BPT...";
    private static final String CONNECTING_AS = "Connecting AS...";

    private static final String INVALID_BPT_DEVICE_TEXT = "Device adress incorrect, this is not a BeamPositionTracking device!";
    private static final String NO_BPT_DEVICE_TEXT = "No BeamPositionTracking device connected yet!";
    private static final String BPT_CONNECTION_FAILURE_TEXT = "<html><body><p>Counldn't connect to distant device</p><p>Please check device adress!</p></body></html>";
    private static final String BPT_CONNECTION_FAILURE = "Failed to connect to BPT";
    private static final String BPT_CONNECTION_INTERRUPTED = "BPT connection interrupted";

    private static final String BPT_DEVICE_CONNECTED = "<html><body>BeamPositionTracking device <b><code>%s</code></b> is connected!</body></html>";

    private static final String DELIMITER = "::";

    private static final Font STATUS_FONT = new Font(Font.DIALOG, Font.PLAIN, 12);
    private static final Font DEVICE_FONT = new Font(Font.MONOSPACED, Font.BOLD, 12);
    private static final CometeFont DEVICE_COMETE_FONT = FontTool.getCometeFont(DEVICE_FONT);

    private ControlPanel controlPanel;

    private StringButton controlRedirection;
    private ProgressDialog progressDialog;

    private ITextTarget bptClassTarget;

    // Right panel
    private Label asDeviceLabel;
    private JTextField bptDeviceTextField;
    private JButton connectBPT;
    private ITextTarget asClassTarget;
    private JLabel bptConnectionStatus;

    private JPanel asDescPanel;
    // Left panel
    private ITextTarget xPid;
    private Label xMinPosition;
    private Label xMaxPosition;
    private Label xCalibrationStepNumber;
    private ITextTarget yPid;
    private Label yMinPosition;
    private Label yMaxPosition;
    private Label yCalibrationStepNumber;
    private Label axesAliases;

    // AS device init
    private Label isXPidInUse;
    private Label xPCoef;
    private Label xICoef;
    private Label xDCoef;
    private Label isYPidInUse;
    private Label yPCoef;
    private Label yICoef;
    private Label yDCoef;

    // specific ui properties
    private Label coldXThreshold;
    private Label hotXThreshold;
    private Label coldYThreshold;
    private Label hotYThreshold;

    // ConfigPanel State, PID info
    private volatile boolean bptReady, asReady, bptDeviceNameChanged;

    public ConfigPanel() {
        super("x", "y", false, false);
        bptDeviceName = ObjectUtils.EMPTY_STRING;
        bptDeviceNameChanged = false;
        bptReady = false;
        asReady = false;
        initComponents();
        initGui();
    }

    @Override
    protected void refreshInterface() {
        EDTManager.INSTANCE.runInDrawingThread(() -> {
            // BPT connection ok?
            boolean visible = bptReady && asReady;
            controlRedirection.setVisible(visible);
            rightPanel.setVisible(visible);
        });
        EDTManager.INSTANCE.runInDrawingThread(() -> {
            // If AS device is ready, display panel
            asDescPanel.setVisible(asReady);
        });
    }

    private void showProgressDialog(String mainMessage, Component relative) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(WindowSwingUtils.getWindowForComponent(connectBPT));
            progressDialog.setProgressIndeterminate(true);
            progressDialog.setTitle(CONNECTING);
        }
        if (!progressDialog.isVisible()) {
            progressDialog.setMainMessage(mainMessage);
            progressDialog.pack();
            progressDialog.setSize(Math.max(progressDialog.getWidth(), 200), progressDialog.getHeight());
            progressDialog.setLocationRelativeTo(relative);
            progressDialog.setVisible(true);
        }
    }

    private void showBPTProgressDialog() {
        showProgressDialog(CONNECTING_BPT, connectBPT);
    }

    private void showASProgressDialog(Window parent) {
        showProgressDialog(CONNECTING_AS, parent);
    }

    /**
     * To init components
     */
    @Override
    protected void initComponents() {
        // States
        super.initComponents();
        // Init control "redirection"
        controlRedirection = new StringButton();
        controlRedirection.setText("BPT Control");
        controlRedirection.addActionListener(evt -> {
            Window parent = WindowSwingUtils.getWindowForComponent(this);
            boolean connectControlPanel = false;
            if (controlPanel == null && !asDeviceLabel.getText().isEmpty()) {
                bptDeviceNameChanged = false;
                showASProgressDialog(parent);
                // Object control panel not created yet
                controlPanel = new ControlPanel(this, xAlias, yAlias);
                connectControlPanel = true;
                // start control panel thread in new thread
                new Thread(controlPanel, "Launch controlPanel").start();
            } else {
                // If control panel started, then show it
                if (bptDeviceNameChanged) {
                    showASProgressDialog(parent);
                    connectControlPanel = true;
                }
            }
            if (connectControlPanel) {
                SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {
                    @Override
                    protected Void doInBackground() throws Exception {
                        controlPanel.updateDevices(bptDeviceTextField.getText(), asDeviceLabel.getText());
                        bptDeviceNameChanged = false;
                        return null;
                    }

                    @Override
                    public void done() {
                        progressDialog.setVisible(false);
                    }
                };
                worker.execute();
            }
            controlPanel.showMainPanel(WindowSwingUtils.getWindowForComponent(this));
        });

        // Devices Address
        bptDeviceTextField = new JTextField(getUserPreference(FAVORITE_BPT_KEY), 20);
        bptDeviceTextField.setFont(DEVICE_FONT);
        bptDeviceTextField.getDocument().addDocumentListener(new BPTDeviceNameListener());

        // connection status
        bptConnectionStatus = new JLabel(NO_BPT_DEVICE_TEXT);
        bptConnectionStatus.setFont(STATUS_FONT);
        // device class
        bptClassTarget = new BPTClassTarget();

        // AS device class
        asClassTarget = new ASClassTarget();

        // axes alias
        axesAliases = new AliasesLabel();

        // Connect BPT button
        connectBPT = new JButton("BPT connection");
        connectBPT.addActionListener(evt -> {
            connectBpt();
        });

        // PIDs
        isXPidInUse = generateValueLabel();
        xPCoef = generateValueLabel();
        xICoef = generateValueLabel();
        xDCoef = generateValueLabel();
        isYPidInUse = generateValueLabel();
        yPCoef = generateValueLabel();
        yICoef = generateValueLabel();
        yDCoef = generateValueLabel();
        // X properties info
        xPid = new PIDTarget(isXPidInUse, xPCoef, xICoef, xDCoef);
        xMinPosition = generateValueLabel();
        xMaxPosition = generateValueLabel();
        xCalibrationStepNumber = generateValueLabel();
        // Y properties info
        yPid = new PIDTarget(isYPidInUse, yPCoef, yICoef, yDCoef);
        yMinPosition = generateValueLabel();
        yMaxPosition = generateValueLabel();
        yCalibrationStepNumber = generateValueLabel();
        // AS
        asDescPanel = new JPanel(new GridLayout(2, 0));
        asDeviceLabel = new ASLabel();
        asDeviceLabel.setCometeFont(DEVICE_COMETE_FONT);
        // UI - specifics
        coldXThreshold = generateValueLabel();
        coldYThreshold = generateValueLabel();
        hotXThreshold = generateValueLabel();
        hotYThreshold = generateValueLabel();
    }

    protected boolean bptDeviceNameChanged() {
        return !bptDeviceName.equalsIgnoreCase(bptDeviceTextField.getText());
    }

    private void connectBpt() {
        showBPTProgressDialog();

        // check if bpt address has changed => to update control panel
        if (bptDeviceNameChanged()) {
            bptDeviceNameChanged = true;
        }

        bptReady = false;
        asReady = false;
        bptConnectionStatus.setText(NO_BPT_DEVICE_TEXT);
        bptDeviceName = bptDeviceTextField.getText();
        bptDeviceTextField.setEnabled(false);
        connectBPT.setEnabled(false);
        SwingWorker<Boolean, Void> worker = new SwingWorker<Boolean, Void>() {

            @Override
            protected Boolean doInBackground() throws Exception {
                disconnectBPTDeviceClass();
                boolean connected = connectBPTDeviceClass();
                return Boolean.valueOf(connected);
            }

            @Override
            protected void done() {
                Boolean connected = Boolean.FALSE;
                try {
                    connected = get();
                } catch (InterruptedException e) {
                    logger.warn(BPT_CONNECTION_INTERRUPTED);
                } catch (ExecutionException e) {
                    logger.error(BPT_CONNECTION_FAILURE, e.getCause());
                } finally {
                    if (!connected.booleanValue()) {
                        bptConnectionStatus.setText(BPT_CONNECTION_FAILURE_TEXT);
                    }
                    bptDeviceTextField.setEnabled(true);
                    progressDialog.setVisible(false);
                }
            }
        };
        setUserPreference(FAVORITE_BPT_KEY, bptDeviceTextField.getText());
        worker.execute();
    }

    private void initializeBPTConnection() {
        closeBPTConnection();
        TangoConnection.connectProperty(TangoConnection.STRING_TYPE, bptDeviceName, X_PID_PROP, xPid);
        TangoConnection.connectProperty(TangoConnection.STRING_TYPE, bptDeviceName, Y_PID_PROP, yPid);
        TangoConnection.connectProperty(TangoConnection.STRING_TYPE, bptDeviceName, X_CALIBRATION_STEP_NUMBER,
                xCalibrationStepNumber);
        TangoConnection.connectProperty(TangoConnection.STRING_TYPE, bptDeviceName, Y_CALIBRATION_STEP_NUMBER,
                yCalibrationStepNumber);
        TangoConnection.connectProperty(TangoConnection.STRING_TYPE, bptDeviceName, AS_DEVICE_ADRESS, asDeviceLabel);
        TangoConnection.connectProperty(TangoConnection.STRING_TYPE, bptDeviceName, AXES_ALIASES, axesAliases);

        TangoConnection.connectProperty(TangoConnection.STRING_TYPE, bptDeviceName, COLD_X_THRESHOLD, coldXThreshold);
        TangoConnection.connectProperty(TangoConnection.STRING_TYPE, bptDeviceName, HOT_X_THRESHOLD, hotXThreshold);
        TangoConnection.connectProperty(TangoConnection.STRING_TYPE, bptDeviceName, COLD_Y_THRESHOLD, coldYThreshold);
        TangoConnection.connectProperty(TangoConnection.STRING_TYPE, bptDeviceName, HOT_Y_THRESHOLD, hotYThreshold);

        bptReady = true;
    }

    private void closeBPTConnection() {
        TangoConnection.disconnectProperty(TangoConnection.STRING_TYPE, xPid);
        TangoConnection.disconnectProperty(TangoConnection.STRING_TYPE, yPid);
        TangoConnection.disconnectProperty(TangoConnection.STRING_TYPE, xCalibrationStepNumber);
        TangoConnection.disconnectProperty(TangoConnection.STRING_TYPE, yCalibrationStepNumber);

        TangoConnection.disconnectProperty(TangoConnection.STRING_TYPE, asDeviceLabel);
        TangoConnection.disconnectProperty(TangoConnection.STRING_TYPE, axesAliases);

        TangoConnection.disconnectProperty(TangoConnection.STRING_TYPE, coldXThreshold);
        TangoConnection.disconnectProperty(TangoConnection.STRING_TYPE, hotXThreshold);
        TangoConnection.disconnectProperty(TangoConnection.STRING_TYPE, coldYThreshold);
        TangoConnection.disconnectProperty(TangoConnection.STRING_TYPE, hotYThreshold);

        bptReady = false;
    }

    private boolean connectBPTDeviceClass() {
        boolean connected = TangoConnection.connectDeviceClass(bptDeviceName, bptClassTarget);
        if (connected) {
            initializeBPTConnection();
        }
        return connected;
    }

    private void disconnectBPTDeviceClass() {
        TangoConnection.disconnectDeviceClass(bptClassTarget);
        closeBPTConnection();
    }

    @Override
    public void setAliases(String xAlias, String yAlias) {
        super.setAliases(xAlias, yAlias);
        if (controlPanel != null) {
            controlPanel.setAliases(xAlias, yAlias);
        }
    }

    private void connectAs(String asDeviceName) {
        disconnectAs();
        if (!TangoConnection.connectDeviceClass(asDeviceName, asClassTarget)) {
            asReady = false;
        }
    }

    private void disconnectAs() {
        TangoConnection.disconnectDeviceClass(asClassTarget);
    }

    private void connectAsElements(String asDeviceName) {
        TangoConnection.connectAttribute(TangoConnection.STRING_TYPE, asDeviceName, TangoAttributeHelper.STATE, asState,
                true);
        // INIT PROPERTIES VALUES
        TangoConnection.connectProperty(TangoConnection.STRING_TYPE, asDeviceName, X_MIN_POSITION, xMinPosition);
        TangoConnection.connectProperty(TangoConnection.STRING_TYPE, asDeviceName, X_MAX_POSITION, xMaxPosition);
        TangoConnection.connectProperty(TangoConnection.STRING_TYPE, asDeviceName, Y_MIN_POSITION, yMinPosition);
        TangoConnection.connectProperty(TangoConnection.STRING_TYPE, asDeviceName, Y_MAX_POSITION, yMaxPosition);
        asReady = true;
    }

    private void disconnectAsElements() {
        TangoConnection.disconnectAttribute(TangoConnection.STRING_TYPE, asState);
        TangoConnection.disconnectProperty(TangoConnection.STRING_TYPE, xMinPosition);
        TangoConnection.disconnectProperty(TangoConnection.STRING_TYPE, xMaxPosition);
        TangoConnection.disconnectProperty(TangoConnection.STRING_TYPE, yMinPosition);
        TangoConnection.disconnectProperty(TangoConnection.STRING_TYPE, yMaxPosition);
        asReady = false;
    }

    @Override
    protected Dimension getPreferredFrameSize() {
        return new Dimension(1500, 600);
    }

    /**
     * To init right panel.
     * 
     * @return A {@link JPanel}.
     */
    @Override
    protected JPanel initRightPanel() {
        JPanel rightPanel = new JPanel(new GridLayout(4, 0));
        Border rightPanelBord = BPTViewUtils.createTitledBorder("BPT Configuration", Color.BLACK);

        // AXES LIMITS PROPERTIES
        JPanel axesLimitsPanel = new JPanel(new GridLayout(0, 2));
        axesLimitsPanel.setBorder(BPTViewUtils.createTitledBorder("AXES LIMITS", BPTViewUtils.DARK_BLUE));
        // X limits
        JPanel xLimitsPanel = new JPanel(new GridLayout(2, 2));
        xLimitsPanel.setBorder(new TitledBorder("X Axis"));
        JLabel xMinLimLab = new JLabel("	Min limit ");
        JLabel xMaxLimLab = new JLabel("	Max limit ");
        xLimitsPanel.add(xMinLimLab);
        xLimitsPanel.add(xMinPosition);
        xLimitsPanel.add(xMaxLimLab);
        xLimitsPanel.add(xMaxPosition);
        // Y limits
        JPanel yLimitsPanel = new JPanel(new GridLayout(2, 2));
        yLimitsPanel.setBorder(new TitledBorder("Y Axis"));
        JLabel yMinLimLab = new JLabel("	Min limit ");
        JLabel yMaxLimLab = new JLabel("	Max limit ");
        yLimitsPanel.add(yMinLimLab);
        yLimitsPanel.add(yMinPosition);
        yLimitsPanel.add(yMaxLimLab);
        yLimitsPanel.add(yMaxPosition);
        axesLimitsPanel.add(xLimitsPanel);
        axesLimitsPanel.add(yLimitsPanel);

        // PID VALUES
        JPanel axesPIDsPanel = new JPanel(new GridLayout(0, 2));
        axesPIDsPanel.setBorder(BPTViewUtils.createTitledBorder("AXES PIDs", BPTViewUtils.DARK_BLUE));

        JLabel pXCoefLab = new JLabel("P coef :");
        JLabel iXCoefLab = new JLabel("I coef :");
        JLabel dXCoefLab = new JLabel("D coef :");
        JPanel xPIDPanel = new JPanel(new GridLayout(0, 3));
        xPIDPanel.setBorder(new TitledBorder("X PID "));

        JPanel xPPanel = new JPanel(new GridLayout(0, 2));
        xPPanel.add(pXCoefLab);
        xPPanel.add(xPCoef);
        JPanel xIPanel = new JPanel(new GridLayout(0, 2));
        xIPanel.add(iXCoefLab);
        xIPanel.add(xICoef);
        JPanel xDPanel = new JPanel(new GridLayout(0, 2));
        xDPanel.add(dXCoefLab);
        xDPanel.add(xDCoef);

        xPIDPanel.add(xPPanel);
        xPIDPanel.add(xIPanel);
        xPIDPanel.add(xDPanel);
        // X limits
        JLabel pYCoefLab = new JLabel("P coef :");
        JLabel iYCoefLab = new JLabel("I coef :");
        JLabel dYCoefLab = new JLabel("D coef :");
        JPanel yPIDPanel = new JPanel(new GridLayout(0, 3));
        yPIDPanel.setBorder(new TitledBorder("Y PID "));

        JPanel yPPanel = new JPanel(new GridLayout(0, 2));
        yPPanel.add(pYCoefLab);
        yPPanel.add(yPCoef);
        JPanel yIPanel = new JPanel(new GridLayout(0, 2));
        yIPanel.add(iYCoefLab);
        yIPanel.add(yICoef);
        JPanel yDPanel = new JPanel(new GridLayout(0, 2));
        yDPanel.add(dYCoefLab);
        yDPanel.add(yDCoef);

        // Axes thresholds
        JPanel thresholdPanel = new JPanel(new GridLayout(0, 2));
        thresholdPanel.setBorder(
                BPTViewUtils.createTitledBorder("Axes thresholds configuration (in pixels)", BPTViewUtils.DARK_BLUE));
        JPanel coldThresholdPanel = new JPanel(new GridLayout(2, 2));
        // cold threshold
        coldThresholdPanel.setBorder(new TitledBorder("Cold beam line"));
        JLabel coldXLabel = new JLabel("X axis ");
        coldThresholdPanel.add(coldXLabel);
        coldThresholdPanel.add(coldXThreshold);
        JLabel coldYLabel = new JLabel("Y axis ");
        coldThresholdPanel.add(coldYLabel);
        coldThresholdPanel.add(coldYThreshold);
        // hot threshold
        JPanel hotThresholdPanel = new JPanel(new GridLayout(2, 2));
        hotThresholdPanel.setBorder(new TitledBorder("Hot beam line"));
        JLabel hotXLabel = new JLabel("X axis ");
        hotThresholdPanel.add(hotXLabel);
        hotThresholdPanel.add(hotXThreshold);
        JLabel hotYLabel = new JLabel("Y axis ");
        hotThresholdPanel.add(hotYLabel);
        hotThresholdPanel.add(hotYThreshold);
        thresholdPanel.add(coldThresholdPanel);
        thresholdPanel.add(hotThresholdPanel);

        yPIDPanel.add(yPPanel);
        yPIDPanel.add(yIPanel);
        yPIDPanel.add(yDPanel);

        axesPIDsPanel.add(xPIDPanel);
        axesPIDsPanel.add(yPIDPanel);

        rightPanel.add(axesLimitsPanel);
        rightPanel.add(axesPIDsPanel);
        rightPanel.add(thresholdPanel);
        rightPanel.setBorder(rightPanelBord);
        return rightPanel;
    }

    /**
     * To init left panel.
     * 
     * @return A {@link JPanel}.
     */
    @Override
    protected JPanel initLeftPanel() {
        JPanel leftPanel = new JPanel(new GridLayout(3, 0));
        Border leftPanelBord = BPTViewUtils.createTitledBorder("Device connections", Color.BLACK);

        // BPT connection
        bptDeviceTextField.setBorder(new TitledBorder("BPT device"));
        JPanel deviceBptconnectionStateButton = new JPanel(new GridLayout(0, 2));
        deviceBptconnectionStateButton.add(connectBPT);
        deviceBptconnectionStateButton.add(bptState);
        JPanel deviceBPTconnection = new JPanel(new GridLayout(3, 0));
        deviceBPTconnection.add(bptDeviceTextField);
        deviceBPTconnection.add(deviceBptconnectionStateButton);
        bptConnectionStatus.setBorder(new TitledBorder("BeamPositionTracking device connection state"));
        deviceBPTconnection.add(bptConnectionStatus);

        asDescPanel.setBorder(new TitledBorder("Actuator system paired"));
        asDescPanel.add(asDeviceLabel);
        asDescPanel.add(asState);

        leftPanel.add(deviceBPTconnection);
        leftPanel.add(asDescPanel);
        leftPanel.add(controlRedirection);
        leftPanel.setBorder(leftPanelBord);
        return leftPanel;
    }

    private String getUserPreference(String prefName) {
        Preferences prefs = Preferences.userRoot().node(this.getClass().getName());
        return prefs.get(prefName, "Enter/Here/BeamPositionDeviceAdress");
    }

    private void setUserPreference(String prefName, String value) {
        Preferences prefs = Preferences.userRoot().node(this.getClass().getName());
        prefs.put(prefName, value);
        try {
            prefs.flush();
        } catch (BackingStoreException e) {
            logger.error("Failed to set user preferences", e);
        }
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected class BPTClassTarget extends ADataTarget {

        public BPTClassTarget() {
            super();
        }

        @Override
        protected boolean isExpectedData(String deviceClass) {
            return BPT_DEVICE_CLASS.equals(deviceClass);
        }

        @Override
        protected void treatNoData() {
            EDTManager.INSTANCE.runInDrawingThread(() -> {
                bptConnectionStatus.setText(NO_BPT_DEVICE_TEXT);
            });
        }

        @Override
        protected void treatInvalidData() {
            EDTManager.INSTANCE.runInDrawingThread(() -> {
                bptConnectionStatus.setText(INVALID_BPT_DEVICE_TEXT);
            });
        }

        @Override
        protected void treatExpectedData() {
            EDTManager.INSTANCE.runInDrawingThread(() -> {
                bptConnectionStatus.setText(String.format(BPT_DEVICE_CONNECTED, bptDeviceName));
            });
        }

    }

    protected class ASClassTarget extends ADataTarget {

        public ASClassTarget() {
            super();
        }

        @Override
        protected boolean isExpectedData(String deviceClass) {
            return true;
        }

        @Override
        protected void treatNoData() {
            disconnectAsElements();
        }

        @Override
        protected void treatInvalidData() {
            // not managed
        }

        @Override
        protected void treatExpectedData() {
            String asDeviceAdress = asDeviceLabel.getText();
            connectAsElements(asDeviceAdress);
        }

    }

    protected class PIDTarget extends ADataTarget {

        private Label isPidInUse, pCoef, iCoef, dCoef;

        public PIDTarget(Label isPidInUse, Label pCoef, Label iCoef, Label dCoef) {
            super();
            this.isPidInUse = isPidInUse;
            this.pCoef = pCoef;
            this.iCoef = iCoef;
            this.dCoef = dCoef;
        }

        @Override
        protected boolean isExpectedData(String data) {
            return true;
        }

        @Override
        protected void treatNoData() {
            // not managed
        }

        @Override
        protected void treatInvalidData() {
            // not managed
        }

        @Override
        protected void treatExpectedData() {
            EDTManager.INSTANCE.runInDrawingThread(() -> {
                try (Scanner s = new Scanner(getText()).useDelimiter(DELIMITER);) {
                    isPidInUse.setText(s.next());
                    pCoef.setText(s.next());
                    iCoef.setText(s.next());
                    dCoef.setText(s.next());
                }
            });
        }

    }

    protected class BPTDeviceNameListener implements DocumentListener {

        protected void treatDocumentEvent(DocumentEvent e) {
            if ((e != null) && (e.getDocument() == bptDeviceTextField.getDocument())) {
                connectBPT.setEnabled(bptDeviceNameChanged());
            }
        }

        @Override
        public void insertUpdate(DocumentEvent e) {
            treatDocumentEvent(e);
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            treatDocumentEvent(e);
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
            treatDocumentEvent(e);
        }

    }

    protected abstract class AConnectionLabel extends Label {

        private static final long serialVersionUID = 4239849469748089856L;

        public AConnectionLabel() {
            super();
        }

        protected abstract void treatDisconection();

        protected abstract void treatConnection(String text);

        @Override
        public final void setText(String text) {
            super.setText(text);
            if ((text == null) || text.isEmpty() || StringScalarBox.DEFAULT_ERROR_STRING.equals(text)) {
                treatDisconection();
            } else {
                treatConnection(text);
            }
        }
    }

    protected class AliasesLabel extends AConnectionLabel {

        private static final long serialVersionUID = 1814414043924918164L;

        public AliasesLabel() {
            super();
        }

        @Override
        protected void treatDisconection() {
            // not managed
        }

        @Override
        protected void treatConnection(String text) {
            String[] aliases = text.split(ObjectUtils.NEW_LINE);
            setAliases(aliases[0], aliases[1]);
        }

    }

    protected class ASLabel extends AConnectionLabel {

        private static final long serialVersionUID = 5252461326468419789L;

        @Override
        protected void treatDisconection() {
            SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {

                @Override
                protected Void doInBackground() throws Exception {
                    disconnectAs();
                    return null;
                }
            };
            worker.execute();
        }

        @Override
        protected void treatConnection(String text) {
            SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {

                @Override
                protected Void doInBackground() throws Exception {
                    connectAs(text);
                    return null;
                }
            };
            worker.execute();
        }

    }

}
