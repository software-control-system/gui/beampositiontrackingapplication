package fr.soleil.gui.bpt.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.Gradient;
import fr.soleil.comete.swing.IconButton;
import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.swing.StringButton;
import fr.soleil.comete.swing.TextArea;
import fr.soleil.comete.swing.WheelSwitch;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.comete.tango.data.service.helper.TangoAttributeHelper;
import fr.soleil.data.target.scalar.ITextTarget;
import fr.soleil.gui.bpt.cometeWrapper.TangoConnection;
import fr.soleil.lib.project.swing.WindowSwingUtils;

public class ControlPanel extends ABPTPanel {

    private static final long serialVersionUID = -442204581344193368L;

    // Connection info
    private static final String SENSOR_TYPE_PROP = "SensorPluginType";
    private static final String IMAGE_ATTR = "thresholdedImage";
    private static final String START_BEAM_TRACKING_CMD = "StartBeamTracking";
    private static final String STOP_BEAM_TRACKING_CMD = "StopBeamTracking";
    private static final String PERCENTAGE_DETECTION_ATTR = "percentageDetection";
    private static final String ORIGINAL_IMAGE_ATTR = "originalImage";
    private static final String X_STATE_ATTR = "xState";
    private static final String Y_STATE_ATTR = "yState";
    private static final String X_POSITION_ATTR = "xPosition";
    private static final String Y_POSITION_ATTR = "yPosition";
    private static final String X_AXIS_CAL_ATTR = "isXCalibrated";
    private static final String Y_AXIS_CAL_ATTR = "isYCalibrated";
    private static final String X_TARGET_ATTR = "xAxisTarget";
    private static final String Y_TARGET_ATTR = "yAxisTarget";
    private static final String X_WARN_ZONE_ATTR = "warningZoneXCenter";
    private static final String Y_WARN_ZONE_ATTR = "warningZoneYCenter";
    private static final String WARN_ZONE_RADIUS_ATTR = "warningZoneRadius";
    private static final String BEAM_X_CENTROID_ATTR = "xAxisCurrentBeamPosition";
    private static final String BEAM_Y_CENTROID_ATTR = "yAxisCurrentBeamPosition";
    private static final String LOCK_MODE_ATTR = "fixedMode";
    private static final String X_THRESHOLD_ATTR = "xAxisRegulationThreshold";
    private static final String Y_THRESHOLD_ATTR = "yAxisRegulationThreshold";
    private static final String COLD_X_THRESHOLD_PROP = "UI_xColdThreshold";
    private static final String HOT_X_THRESHOLD_PROP = "UI_xHotThreshold";
    private static final String COLD_Y_THRESHOLD_PROP = "UI_yColdThreshold";
    private static final String HOT_Y_THRESHOLD_PROP = "UI_yHotThreshold";
    private static final String DEVICE_LOCK_MODE_STR = "Static positionning mode!";
    private static final String DEVICE_DYN_MODE_STR = "Dynamic positionning mode!";

    // GUI texts
    private static final String IMAGE_SENSOR_TYPE = "Lima"; // To change: plugin name
    private static final String THRESHOLD = "- Threshold (%) :";
    private static final String IMAGE_THRESHOLD = "IMAGE THRESHOLD";
    private static final String IMAGE = "Image";
    private static final String THRESHOLDED_IMAGE = "Thresholded Image";
    private static final String ORIGINAL_IMAGE = "Original Image";
    private static final String START_BEAM_TRACKING = "Start Beam Tracking";
    private static final String STOP_BEAM_TRACKING = "Stop Beam Tracking";
    private static final String BEAMLINE_IS_CURRENTLY_IN_COLD_MODE = "Beamline is currently in Cold mode";
    private static final String BEAMLINE_IS_CURRENTLY_IN_HOT_MODE = "Beamline is currently in Hot mode";
    private static final String BEAMLINE_IS_CURRENTLY_IN_SPECIFIC_MODE = "Beamline is currently in specific mode";
    private static final String SET_HOT_MODE = "Set Hot mode";
    private static final String SET_COLD_MODE = "Set Cold mode";
    private static final String A_NEW_TARGET_HAS_BEEN_SELECTED_ON_IMAGE = "A new target has been selected on image : \nX = %s, Y = %s\nSend beam to this new target?";
    private static final String WARNING = "Warning";
    private static final String BPT_CONFIGURATION = "BPT Configuration";
    private static final String BEAM_CONTROL = "Beam Control";
    private static final String BEAM_CENTROID_STATE = "BEAM CENTROID STATE";
    private static final String CENTROIDS = "CENTROIDS";
    private static final String CURRENT_CENTROID = "- %s Current Centroid ";
    private static final String TARGETS = "TARGETS";
    private static final String CURRENT_TARGET = "- %s Current Target ";
    private static final String WARNING_ZONE = "WARNING ZONE";
    private static final String CENTER = " center";
    private static final String RADIUS = "Radius";
    private static final String ASSERVISSEMENT_LIMITS = "ASSERVISSEMENT LIMITS";
    private static final String BEAMLINE_THRESHOLD_STATUS = "BEAMLINE THRESHOLD STATUS";
    private static final String BEAM_POSITION_TRACKING_DEVICE_STATUS = "Beam Position Tracking device status";
    private static final String ACTUATOR_SYSTEM_DEVICE_STATUS = "Actuator system device status";
    private static final String AXIS_NOT_CALIBRATED = "Axis Not Calibrated";
    private static final String AXIS_CALIBRATED = "Axis Calibrated";
    private static final String AXIS_STATUS = " AXIS STATUS";
    private static final String BEAM_POSITION = "Beam Position";

    // Values
    private static final String ON = "ON";
    private static final String RUNNING = "RUNNING";
    private static final String TRUE = "TRUE";
    private static final String VERDANA = "Verdana";

    private boolean bptReadyToOperate;
    private boolean bptRunning;
    private ConfigPanel configPanel;
    private int previousX;
    private int previousY;
    private ITextTarget sensorType;
    // COMPONENTS - LEFT PANEL
    private ImageViewer viewer;
    private final TitledBorder originalImageBorder;
    private final TitledBorder thresholdedImageBorder;
    private StringButton configPanelButton;
    private StringButton startBTButtonCMD;
    private StringButton stopBTButtonCMD;
    private StringButton trackingButton;
    private Label xAxisState;
    private Label xAxisPosition;
    private IconButton isXAxisCalibrated;
    private Label yAxisState;
    private Label yAxisPosition;
    private IconButton isYAxisCalibrated;
    private JPanel xStatePanel, yStatePanel;

    // COMPONENTS - RIGHT PANEL
    private final JPanel detectorPanel, thresholdPanel;
    private JLabel thresholdLabel;
    private TextArea bptStatus;
    private TextArea asStatus;
    private WheelSwitch xTarget;
    private WheelSwitch yTarget;
    private WheelSwitch xWarnCenter;
    private WheelSwitch yWarnCenter;
    private WheelSwitch warnRadius;
    private JLabel xcentroidLab, ycentroidLab, xTargetLab, yTargetLab;
    private JPanel warningZoneXPanel, warningZoneYPanel;
    private Label xCentroid, yCentroid;
    private IconButton lockModeStatus;
    // Specific Lima plugin

    private IconButton originalImage;
    private WheelSwitch percentageDetection;
    // axes thresholds
    private Label currentThresholdMode;
    private WheelSwitch currentXThreshold;
    private WheelSwitch currentYThreshold;
    private WheelSwitch coldXThreshold;
    private WheelSwitch hotXThreshold;
    private WheelSwitch coldYThreshold;
    private WheelSwitch hotYThreshold;
    private boolean coldBeamLineMode;
    private boolean hotBeamLineMode;
    private StringButton switchBeamLineThresholdMode;

    private Label lockStatus;
    private boolean lockMode;

    /**
     * Creates a new control panel
     * 
     * @param bptDeviceAdress
     * @param configPanel
     * @param xAlias
     * @param yAlias
     */
    public ControlPanel(ConfigPanel configPanel, String xAlias, String yAlias) {
        super(xAlias, yAlias, false, false);
        this.configPanel = configPanel;
        originalImageBorder = BPTViewUtils.createTitledBorder(ORIGINAL_IMAGE, BPTViewUtils.DARK_BLUE);
        thresholdedImageBorder = BPTViewUtils.createTitledBorder(THRESHOLDED_IMAGE, BPTViewUtils.DARK_BLUE);
        detectorPanel = new JPanel(new GridLayout(2, 0));
        thresholdPanel = new JPanel(new GridLayout(2, 1));
        bptReadyToOperate = false;
        bptRunning = false;
        coldBeamLineMode = false;
        hotBeamLineMode = false;
        lockMode = true;
        initComponents();
        initGui();
    }

    /**
     * To update devices in use.
     * 
     * @param bptDeviceAdress
     * @param asDeviceAdress
     * @return Whether connections are done.
     */
    public boolean updateDevices(String bptDeviceAdress, String asDeviceAdress) {
        this.bptDeviceName = bptDeviceAdress;
        boolean updated;
        try {
            initTangoConnexions(bptDeviceAdress, asDeviceAdress);
            updated = true;
        } catch (Exception e) {
            logger.warn("FAILED TO UPDATE TANGO CONNEXIONS", e);
            updated = false;
        }
        return updated;
    }

    private void setHotBeamLineMode() {
        currentXThreshold.setValue(hotXThreshold.getValue(), true, true);
        currentYThreshold.setValue(hotYThreshold.getValue(), true, true);

        switchBeamLineThresholdMode.setText(SET_COLD_MODE);
    }

    private void setColdBeamLineMode() {
        currentXThreshold.setValue(coldXThreshold.getValue(), true, true);
        currentYThreshold.setValue(coldYThreshold.getValue(), true, true);

        switchBeamLineThresholdMode.setText(SET_COLD_MODE);
    }

    /**
     * To init specific properties.
     */
    private void connectSpecificProperties() {
        disconnectSpecificProperties();
        TangoConnection.connectAttribute(TangoConnection.NUMBER_TYPE, bptDeviceName, PERCENTAGE_DETECTION_ATTR,
                percentageDetection, false);
        TangoConnection.connectAttribute(TangoConnection.BOOLEAN_TYPE, bptDeviceName, ORIGINAL_IMAGE_ATTR,
                originalImage, false);
    }

    private void disconnectSpecificProperties() {
        TangoConnection.disconnectAttribute(TangoConnection.NUMBER_TYPE, percentageDetection);
        TangoConnection.disconnectAttribute(TangoConnection.BOOLEAN_TYPE, originalImage);
    }

    /**
     * To refresh interface
     */
    @Override
    protected void refreshInterface() {
        refreshBPTState();
        refreshApplicationCommands();
        refreshThresholdMode();
        if (!lockMode) {
            updateSelectedTargetOnImage();
        }
    }

    private void updateImageConfiguration(boolean original) {
        Gradient gradient = new Gradient();
        Color beamColor;
        Border imageTitle;
        if (original) {
            gradient.buildRainbowGradient();
            beamColor = Color.BLACK;
            imageTitle = originalImageBorder;
        } else {
            gradient.buildMonochromeGradient();
            beamColor = Color.RED;
            imageTitle = thresholdedImageBorder;
        }
        viewer.setGradient(gradient);
        viewer.setBeamColor(beamColor);
        EDTManager.INSTANCE.runInDrawingThread(() -> {
            viewer.setBorder(imageTitle);
        });
    }

    /**
     * Is beam line in Hot/Cold mode?
     */
    private void refreshThresholdMode() {
        if ((coldXThreshold.getValue() == currentXThreshold.getValue())
                && (coldYThreshold.getValue() == currentYThreshold.getValue())) {
            // Beam line is in cold mode
            EDTManager.INSTANCE.runInDrawingThread(() -> {
                currentThresholdMode.setText(BEAMLINE_IS_CURRENTLY_IN_COLD_MODE);
                switchBeamLineThresholdMode.setText(SET_HOT_MODE);
            });
            coldBeamLineMode = true;
            hotBeamLineMode = false;
        } else if ((hotXThreshold.getValue() == currentXThreshold.getValue())
                && (hotYThreshold.getValue() == currentYThreshold.getValue())) {
            // Beam line is in hot mode
            EDTManager.INSTANCE.runInDrawingThread(() -> {
                currentThresholdMode.setText(BEAMLINE_IS_CURRENTLY_IN_HOT_MODE);
                switchBeamLineThresholdMode.setText(SET_COLD_MODE);
            });
            hotBeamLineMode = true;
            coldBeamLineMode = false;
        } else {
            // Beam line is in specific mode
            EDTManager.INSTANCE.runInDrawingThread(() -> {
                currentThresholdMode.setText(BEAMLINE_IS_CURRENTLY_IN_SPECIFIC_MODE);
                switchBeamLineThresholdMode.setText(SET_COLD_MODE);
            });
            hotBeamLineMode = false;
            coldBeamLineMode = false;
        }
    }

    private void updateSelectedTargetOnImage() {
        // Get Selected coordinates
        int selectedX = (int) viewer.getSelectedX();
        int selectedY = (int) viewer.getSelectedY();
        boolean newTarget = false;
        // Check if there is a new target
        if ((selectedX != previousX) || (selectedY != previousY)) {
            previousY = selectedY;
            previousX = selectedX;
            newTarget = true;
        }
        // If a new target was registered, notify user before sending axes...
        if (newTarget) {
            int confirmNewTarget = JOptionPane.showConfirmDialog(null,
                    String.format(A_NEW_TARGET_HAS_BEEN_SELECTED_ON_IMAGE, selectedX, selectedY), WARNING,
                    JOptionPane.YES_NO_OPTION);
            if (confirmNewTarget == JOptionPane.YES_OPTION) {
                // User confirmation -> sending beam to the new target
                EDTManager.INSTANCE.runInDrawingThread(() -> {
                    xTarget.setValue(selectedX, true, true);
                    yTarget.setValue(selectedY, true, true);
                });
            }
        }
    }

    private void updateTargetDraw() {
        if ((viewer != null) && (xTarget != null) && (yTarget != null)) {
            // draw target on image
            viewer.setBeamPoint(xTarget.getValue(), yTarget.getValue());
        }
    }

    private void refreshBPTState() {
        String bptStateStr = bptState.getText();
        if (ON.equalsIgnoreCase(bptStateStr)) {
            bptReadyToOperate = true;
            bptRunning = false;
        } else if (RUNNING.equalsIgnoreCase(bptStateStr)) {
            bptReadyToOperate = false;
            bptRunning = true;
        } else {
            bptReadyToOperate = false;
            bptRunning = false;
        }

        // refresh lock state
        lockMode = TRUE.equalsIgnoreCase(lockModeStatus.getText());
        EDTManager.INSTANCE.runInDrawingThread(() -> {
            boolean editable;
            if (lockMode) {
                lockStatus.setText(DEVICE_LOCK_MODE_STR);
                editable = false;
            } else {
                lockStatus.setText(DEVICE_DYN_MODE_STR);
                editable = true;
            }
            xTarget.setEditable(editable);
            yTarget.setEditable(editable);
            warnRadius.setEditable(editable);
            xWarnCenter.setEditable(editable);
            yWarnCenter.setEditable(editable);
            percentageDetection.setEditable(editable);
        });
    }

    private void refreshApplicationCommands() {
        EDTManager.INSTANCE.runInDrawingThread(() -> {
            if (bptReadyToOperate && !bptRunning) {
                trackingButton.setVisible(true);
                if (!START_BEAM_TRACKING.equals(trackingButton.getText())) {
                    trackingButton.setText(START_BEAM_TRACKING);
                }
            } else if (bptRunning) {
                trackingButton.setVisible(true);
                if (!STOP_BEAM_TRACKING.equals(trackingButton.getText())) {
                    trackingButton.setText(STOP_BEAM_TRACKING);
                }
            } else {
                trackingButton.setVisible(false);
            }
        });
    }

    /**
     * To init components
     */
    @Override
    protected void initComponents() {
        // BPT/AS State
        super.initComponents();
        // Image component
        viewer = new BPTImageViewer();
        // Control access
        configPanelButton = new StringButton();
        configPanelButton.setText(BPT_CONFIGURATION);
        configPanelButton.addActionListener(evt -> {
            configPanel.showMainPanel(WindowSwingUtils.getWindowForComponent(this));
        });
        // Hot beam line
        switchBeamLineThresholdMode = new StringButton();
        switchBeamLineThresholdMode.setText(SET_COLD_MODE);
        switchBeamLineThresholdMode.addActionListener(evt -> {
            if (hotBeamLineMode) {
                setColdBeamLineMode();
            } else if (coldBeamLineMode) {
                setHotBeamLineMode();
            } else {
                setColdBeamLineMode();
            }
        });

        // Starts and stops tracking
        trackingButton = new StringButton();
        trackingButton.setText(START_BEAM_TRACKING);
        trackingButton.addActionListener(evt -> {
            if (bptRunning) {
                stopBTButtonCMD.doClick();
            } else if (!bptRunning && bptReadyToOperate) {
                startBTButtonCMD.doClick();
            }
        });

        Font boldFontTarget = new Font(VERDANA, Font.BOLD, 20);
        Font boldFontCentroid = new Font(VERDANA, Font.BOLD, 15);

        sensorType = new SensorTypeTarget();
        // BPT Status
        bptStatus = new TextArea();
        // AS Status
        asStatus = new TextArea();
        // start beam tracking button
        startBTButtonCMD = new StringButton();
        // stop beam tracking button
        stopBTButtonCMD = new StringButton();
        // States
        xAxisState = new Label();
        yAxisState = new Label();
        // Axes positions
        xAxisPosition = generateValueLabel();
        yAxisPosition = generateValueLabel();
        // Axes calibration state
        isXAxisCalibrated = new IconButton();
        isYAxisCalibrated = new IconButton();
        // Percentage detection
        percentageDetection = new WheelSwitch();
        originalImage = new ImageTypeButton();
        // Beam targets
        xTarget = new TargetSwitch();
        xTarget.setFont(boldFontTarget);
        yTarget = new TargetSwitch();
        yTarget.setFont(boldFontTarget);
        // Warning zone
        xWarnCenter = new WheelSwitch();
        yWarnCenter = new WheelSwitch();
        warnRadius = new WheelSwitch();
        // Beam Centroid
        xCentroid = new Label();
        xCentroid.setFont(boldFontCentroid);
        xCentroid.setCometeBackground(CometeColor.CYAN);
        yCentroid = generateValueLabel();

        yCentroid.setCometeBackground(CometeColor.CYAN);
        yCentroid.setFont(boldFontCentroid);
        // lock mode
        lockModeStatus = new IconButton();
        lockStatus = generateValueLabel();
        // axes thresholds
        currentXThreshold = new WheelSwitch();
        currentYThreshold = new WheelSwitch();
        coldXThreshold = new WheelSwitch();
        hotXThreshold = new WheelSwitch();
        coldYThreshold = new WheelSwitch();
        hotYThreshold = new WheelSwitch();
        currentThresholdMode = generateValueLabel();
        // specific properties
        thresholdLabel = new JLabel(THRESHOLD);
        thresholdPanel.add(thresholdLabel);
        thresholdPanel.setBorder(new TitledBorder(IMAGE_THRESHOLD));
        thresholdPanel.add(percentageDetection);
        // original image option button
        thresholdPanel.add(originalImage);
        detectorPanel.add(thresholdPanel);
    }

    /**
     * To init tango connexions
     * 
     * @param bptDeviceAdress
     * @param asDeviceAdress
     */
    private void initTangoConnexions(String bptDeviceAdress, String asDeviceAdress) {
        closeTangoConnections();
        // Image connection
        TangoConnection.connectAttribute(bptDeviceAdress, IMAGE_ATTR, viewer);
        // Connect BPT state
        TangoConnection.connectAttribute(TangoConnection.STRING_TYPE, bptDeviceAdress, TangoAttributeHelper.STATE,
                bptState, true);
        // Connect BPT status
        TangoConnection.connectAttribute(TangoConnection.STRING_TYPE, bptDeviceAdress, TangoAttributeHelper.STATUS,
                bptStatus, false);
        // Connect AS state
        TangoConnection.connectAttribute(TangoConnection.STRING_TYPE, asDeviceAdress, TangoAttributeHelper.STATE,
                asState, true);
        // Connect AS status
        TangoConnection.connectAttribute(TangoConnection.STRING_TYPE, asDeviceAdress, TangoAttributeHelper.STATUS,
                asStatus, false);
        // Connect BeamTracking commands
        TangoConnection.connectCommand(TangoConnection.STRING_TYPE, bptDeviceAdress, START_BEAM_TRACKING_CMD,
                startBTButtonCMD, false);
        TangoConnection.connectCommand(TangoConnection.STRING_TYPE, bptDeviceAdress, STOP_BEAM_TRACKING_CMD,
                stopBTButtonCMD, false);
        // Connect axes states Labels
        TangoConnection.connectAttribute(TangoConnection.STRING_TYPE, asDeviceAdress, X_STATE_ATTR, xAxisState, true);
        TangoConnection.connectAttribute(TangoConnection.STRING_TYPE, asDeviceAdress, Y_STATE_ATTR, yAxisState, true);
        // Connect axes positions Labels
        TangoConnection.connectAttribute(TangoConnection.STRING_TYPE, asDeviceAdress, X_POSITION_ATTR, xAxisPosition,
                false);
        TangoConnection.connectAttribute(TangoConnection.STRING_TYPE, asDeviceAdress, Y_POSITION_ATTR, yAxisPosition,
                false);
        // Connect axes calibration states
        TangoConnection.connectAttribute(TangoConnection.BOOLEAN_TYPE, asDeviceAdress, X_AXIS_CAL_ATTR,
                isXAxisCalibrated, false);
        TangoConnection.connectAttribute(TangoConnection.BOOLEAN_TYPE, asDeviceAdress, Y_AXIS_CAL_ATTR,
                isYAxisCalibrated, false);
        // Connect targets
        TangoConnection.connectAttribute(TangoConnection.NUMBER_TYPE, bptDeviceAdress, X_TARGET_ATTR, xTarget, false);
        TangoConnection.connectAttribute(TangoConnection.NUMBER_TYPE, bptDeviceAdress, Y_TARGET_ATTR, yTarget, false);
        // Connect warningZone
        TangoConnection.connectAttribute(TangoConnection.NUMBER_TYPE, bptDeviceAdress, X_WARN_ZONE_ATTR, xWarnCenter,
                false);
        TangoConnection.connectAttribute(TangoConnection.NUMBER_TYPE, bptDeviceAdress, Y_WARN_ZONE_ATTR, yWarnCenter,
                false);
        TangoConnection.connectAttribute(TangoConnection.NUMBER_TYPE, bptDeviceAdress, WARN_ZONE_RADIUS_ATTR,
                warnRadius, false);
        // Connect centroids
        TangoConnection.connectAttribute(TangoConnection.STRING_TYPE, bptDeviceAdress, BEAM_X_CENTROID_ATTR, xCentroid,
                false);
        TangoConnection.connectAttribute(TangoConnection.STRING_TYPE, bptDeviceAdress, BEAM_Y_CENTROID_ATTR, yCentroid,
                false);
        // Connect lockMode
        TangoConnection.connectAttribute(TangoConnection.BOOLEAN_TYPE, bptDeviceAdress, LOCK_MODE_ATTR, lockModeStatus,
                false);
        // Connect thresholds
        TangoConnection.connectAttribute(TangoConnection.NUMBER_TYPE, bptDeviceAdress, X_THRESHOLD_ATTR,
                currentXThreshold, false);
        TangoConnection.connectAttribute(TangoConnection.NUMBER_TYPE, bptDeviceAdress, Y_THRESHOLD_ATTR,
                currentYThreshold, false);
        TangoConnection.connectProperty(TangoConnection.NUMBER_TYPE, bptDeviceAdress, COLD_X_THRESHOLD_PROP,
                coldXThreshold);
        TangoConnection.connectProperty(TangoConnection.NUMBER_TYPE, bptDeviceAdress, COLD_Y_THRESHOLD_PROP,
                coldYThreshold);
        TangoConnection.connectProperty(TangoConnection.NUMBER_TYPE, bptDeviceAdress, HOT_X_THRESHOLD_PROP,
                hotXThreshold);
        TangoConnection.connectProperty(TangoConnection.NUMBER_TYPE, bptDeviceAdress, HOT_Y_THRESHOLD_PROP,
                hotYThreshold);
        // Sensor type : used to initialized specific stuff in UI
        TangoConnection.connectProperty(TangoConnection.STRING_TYPE, bptDeviceAdress, SENSOR_TYPE_PROP, sensorType);
    }

    private void closeTangoConnections() {
        // Image connection
        TangoConnection.disconnectAttribute(viewer);
        // Connect BPT state
        TangoConnection.disconnectAttribute(TangoConnection.STRING_TYPE, bptState);
        // Connect BPT status
        TangoConnection.disconnectAttribute(TangoConnection.STRING_TYPE, bptStatus);
        // Connect AS state
        TangoConnection.disconnectAttribute(TangoConnection.STRING_TYPE, asState);
        // Connect AS status
        TangoConnection.disconnectAttribute(TangoConnection.STRING_TYPE, asStatus);
        // Connect BeamTracking commands
        TangoConnection.disconnectCommand(TangoConnection.STRING_TYPE, startBTButtonCMD);
        TangoConnection.disconnectCommand(TangoConnection.STRING_TYPE, stopBTButtonCMD);
        // Connect axes states Labels
        TangoConnection.disconnectAttribute(TangoConnection.STRING_TYPE, xAxisState);
        TangoConnection.disconnectAttribute(TangoConnection.STRING_TYPE, yAxisState);
        // Connect axes positions Labels
        TangoConnection.disconnectAttribute(TangoConnection.STRING_TYPE, xAxisPosition);
        TangoConnection.disconnectAttribute(TangoConnection.STRING_TYPE, yAxisPosition);
        // Connect axes calibration states
        TangoConnection.disconnectAttribute(TangoConnection.BOOLEAN_TYPE, isXAxisCalibrated);
        TangoConnection.disconnectAttribute(TangoConnection.BOOLEAN_TYPE, isYAxisCalibrated);
        // Connect targets
        TangoConnection.disconnectAttribute(TangoConnection.NUMBER_TYPE, xTarget);
        TangoConnection.disconnectAttribute(TangoConnection.NUMBER_TYPE, yTarget);
        // Connect warningZone
        TangoConnection.disconnectAttribute(TangoConnection.NUMBER_TYPE, xWarnCenter);
        TangoConnection.disconnectAttribute(TangoConnection.NUMBER_TYPE, yWarnCenter);
        TangoConnection.disconnectAttribute(TangoConnection.NUMBER_TYPE, warnRadius);
        // Connect centroids
        TangoConnection.disconnectAttribute(TangoConnection.STRING_TYPE, xCentroid);
        TangoConnection.disconnectAttribute(TangoConnection.STRING_TYPE, yCentroid);
        // Connect lockMode
        TangoConnection.disconnectAttribute(TangoConnection.BOOLEAN_TYPE, lockModeStatus);
        // Connect thresholds
        TangoConnection.disconnectAttribute(TangoConnection.NUMBER_TYPE, currentXThreshold);
        TangoConnection.disconnectAttribute(TangoConnection.NUMBER_TYPE, currentYThreshold);
        TangoConnection.disconnectProperty(TangoConnection.NUMBER_TYPE, coldXThreshold);
        TangoConnection.disconnectProperty(TangoConnection.NUMBER_TYPE, coldYThreshold);
        TangoConnection.disconnectProperty(TangoConnection.NUMBER_TYPE, hotXThreshold);
        TangoConnection.disconnectProperty(TangoConnection.NUMBER_TYPE, hotYThreshold);
        // Sensor type : used to initialized specific stuff in UI
        TangoConnection.disconnectProperty(TangoConnection.STRING_TYPE, sensorType);
    }

    @Override
    protected Dimension getPreferredFrameSize() {
        return new Dimension(1500, 950);
    }

    @Override
    public void setAliases(String xAlias, String yAlias) {
        super.setAliases(xAlias, yAlias);
        if ((leftPanel != null) && (rightPanel != null)) {
            EDTManager.INSTANCE.runInDrawingThread(() -> {
                xStatePanel.setBorder(BPTViewUtils.createTitledBorder(xAlias + AXIS_STATUS, BPTViewUtils.DARK_BLUE));
                yStatePanel.setBorder(BPTViewUtils.createTitledBorder(yAlias + AXIS_STATUS, BPTViewUtils.DARK_BLUE));

                xcentroidLab.setText(String.format(CURRENT_CENTROID, xAlias));
                ycentroidLab.setText(String.format(CURRENT_CENTROID, yAlias));
                xTargetLab.setText(String.format(CURRENT_TARGET, xAlias));
                yTargetLab.setText(String.format(CURRENT_TARGET, yAlias));
                warningZoneXPanel.setBorder(new TitledBorder(xAlias + CENTER));
                warningZoneYPanel.setBorder(new TitledBorder(yAlias + CENTER));
            });
        }
    }

    /**
     * To init right panel
     * 
     * @return A {@link JPanel}
     */
    @Override
    protected JPanel initRightPanel() {
        // IMAGE PANEL INIT
        JPanel rightPanel = new JPanel(new GridLayout(4, 0));
        Border imagePanBord = BPTViewUtils.createTitledBorder(BEAM_CONTROL, Color.BLACK);

        // BEAM TARGET DEFINITION
        JPanel centroidPanel = new JPanel(new GridLayout(0, 2));
        centroidPanel.setBorder(BPTViewUtils.createTitledBorder(BEAM_CENTROID_STATE, BPTViewUtils.DARK_BLUE));
        // Current Beam
        JPanel currentCentroidPanel = new JPanel(new GridLayout(2, 2));
        currentCentroidPanel.setBorder(new TitledBorder(CENTROIDS));
        xcentroidLab = new JLabel(String.format(CURRENT_CENTROID, xAlias));
        ycentroidLab = new JLabel(String.format(CURRENT_CENTROID, yAlias));
        currentCentroidPanel.add(xcentroidLab);
        currentCentroidPanel.add(ycentroidLab);
        currentCentroidPanel.add(xCentroid);
        currentCentroidPanel.add(yCentroid);

        detectorPanel.add(currentCentroidPanel);

        // Targeted beam
        JPanel targetCentroidPanel = new JPanel(new GridLayout(2, 2));
        targetCentroidPanel.setBorder(new TitledBorder(TARGETS));
        // Target labels definitions
        xTargetLab = new JLabel(String.format(CURRENT_TARGET, xAlias));
        yTargetLab = new JLabel(String.format(CURRENT_TARGET, yAlias));
        targetCentroidPanel.add(xTargetLab);
        targetCentroidPanel.add(yTargetLab);
        JPanel xtargetPanel = new JPanel(new BorderLayout());
        xtargetPanel.add(xTarget);
        xTarget.setPreferredSize(xtargetPanel.getSize());
        targetCentroidPanel.add(xtargetPanel);
        targetCentroidPanel.add(yTarget);
        // Adding to centroid panel
        centroidPanel.add(detectorPanel);
        centroidPanel.add(targetCentroidPanel);

        // ASSERVISSEMENT LIMITS
        JPanel asservissementLimits = new JPanel(new GridLayout(0, 2));
        // WARNING ZONE DEFINITION
        JPanel warningZonePanel = new JPanel(new GridLayout(3, 0));
        warningZonePanel.setBorder(BPTViewUtils.createTitledBorder(WARNING_ZONE, BPTViewUtils.DARK_RED));
        // Panel construction
        warningZoneXPanel = new JPanel(new GridLayout(0, 1));
        warningZoneXPanel.setBorder(new TitledBorder(xAlias + CENTER));
        warningZoneXPanel.add(xWarnCenter);
        warningZoneYPanel = new JPanel(new GridLayout(0, 1));
        warningZoneYPanel.setBorder(new TitledBorder(yAlias + CENTER));
        warningZoneYPanel.add(yWarnCenter);
        JPanel warningZoneRadPanel = new JPanel(new GridLayout(0, 1));
        warningZoneRadPanel.setBorder(new TitledBorder(RADIUS));
        warningZoneRadPanel.add(warnRadius);
        warningZonePanel.add(warningZoneXPanel);
        warningZonePanel.add(warningZoneYPanel);
        warningZonePanel.add(warningZoneRadPanel);
        asservissementLimits.setBorder(BPTViewUtils.createTitledBorder(ASSERVISSEMENT_LIMITS, BPTViewUtils.DARK_BLUE));
        asservissementLimits.add(warningZonePanel);
        JPanel thresholdsPannel = new JPanel(new GridLayout(2, 0));
        thresholdsPannel.setBorder(BPTViewUtils.createTitledBorder(BEAMLINE_THRESHOLD_STATUS, BPTViewUtils.DARK_RED));
        thresholdsPannel.add(currentThresholdMode);
        thresholdsPannel.add(switchBeamLineThresholdMode);
        asservissementLimits.add(thresholdsPannel);

        // BPT SATE/STATUS DEFINITION
        JPanel bptStatusPanel = new JPanel(new GridLayout(0, 2));
        Border bptStatusBrd = BPTViewUtils.createTitledBorder(BEAM_POSITION_TRACKING_DEVICE_STATUS,
                BPTViewUtils.DARK_BLUE);
        bptStatusPanel.setBorder(bptStatusBrd);
        JPanel bptStatePanel = new JPanel(new GridLayout(2, 0));
        bptStatePanel.add(bptState);

        if (TRUE.equals(lockModeStatus.getText())) {
            lockStatus.setText(DEVICE_LOCK_MODE_STR);
        } else {
            lockStatus.setText(DEVICE_DYN_MODE_STR);
        }
        bptStatePanel.add(lockStatus);
        bptStatusPanel.add(bptStatePanel);
        bptStatusPanel.add(bptStatus);

        // AS SATE/STATUS DEFINITION
        JPanel asStatusPanel = new JPanel(new GridLayout(0, 2));
        Border asStatusBrd = BPTViewUtils.createTitledBorder(ACTUATOR_SYSTEM_DEVICE_STATUS, BPTViewUtils.DARK_BLUE);
        asStatusPanel.setBorder(asStatusBrd);
        asStatusPanel.add(asState);
        asStatusPanel.add(asStatus);

        rightPanel.setBorder(imagePanBord);
        rightPanel.add(centroidPanel);
        rightPanel.add(asservissementLimits);
        rightPanel.add(bptStatusPanel);
        rightPanel.add(asStatusPanel);

        return rightPanel;
    }

    /**
     * To init left panel
     * 
     * @return A {@link JPanel}
     */
    @Override
    protected JPanel initLeftPanel() {
        // IMAGE PANEL INIT
        JPanel leftPanel = new JPanel(new BorderLayout());
        viewer.setAllowBeamPositionByClick(true);
        viewer.setAutoZoom(true);
        viewer.setAutoBestFit(true);
        Border imgBrd = new TitledBorder(IMAGE);
        viewer.setBorder(imgBrd);
        // CMD PANNEL INIT
        JPanel cmdPanel = new JPanel(new GridLayout(4, 0));
        // state panel
        xStatePanel = new JPanel(new GridLayout(0, 3));
        yStatePanel = new JPanel(new GridLayout(0, 3));
        isXAxisCalibrated.setFalseLabel(AXIS_NOT_CALIBRATED);
        isXAxisCalibrated.setTrueLabel(AXIS_CALIBRATED);
        isXAxisCalibrated.setBackground(Color.LIGHT_GRAY);
        isYAxisCalibrated.setFalseLabel(AXIS_NOT_CALIBRATED);
        isYAxisCalibrated.setTrueLabel(AXIS_CALIBRATED);
        isYAxisCalibrated.setBackground(Color.LIGHT_GRAY);
        // Axes states
        xStatePanel.add(xAxisState);
        xStatePanel.add(xAxisPosition);
        // Label xUnit = generateValueLabel();
        // xStatePanel.add(xUnit);
        xStatePanel.add(isXAxisCalibrated);
        yStatePanel.add(yAxisState);
        yStatePanel.add(yAxisPosition);
        // Label yUnit = generateValueLabel();
        // yStatePanel.add(yUnit);
        yStatePanel.add(isYAxisCalibrated);
        xStatePanel.setBorder(BPTViewUtils.createTitledBorder(xAlias + AXIS_STATUS, BPTViewUtils.DARK_BLUE));
        yStatePanel.setBorder(BPTViewUtils.createTitledBorder(yAlias + AXIS_STATUS, BPTViewUtils.DARK_BLUE));

        cmdPanel.add(xStatePanel);
        cmdPanel.add(yStatePanel);
        cmdPanel.add(trackingButton);
        cmdPanel.add(configPanelButton);

        leftPanel.setBorder(BPTViewUtils.createTitledBorder(BEAM_POSITION, Color.BLACK));
        leftPanel.add(viewer, BorderLayout.CENTER);
        leftPanel.add(cmdPanel, BorderLayout.SOUTH);

        return leftPanel;
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected class BPTImageViewer extends ImageViewer {

        private static final long serialVersionUID = 1255791592242440911L;

        public BPTImageViewer() {
            super();
            setAlwaysFitMaxSize(true);
            setBeamPointEnabled(true);
            setDrawBeamPosition(true);
        }

        @Override
        protected void initSelectionMenu() {
            super.initSelectionMenu();
            selectionMenu.add(containerFactory.createMenuItem(getAction(MODE_ANGLE_ACTION)));
        }

    }

    protected class ImageTypeButton extends IconButton {

        private static final long serialVersionUID = 5818921830705330207L;

        public ImageTypeButton() {
            super();
            setTrueIconId(0);
            setFalseIconId(1);
            setTrueLabel(ORIGINAL_IMAGE);
            setFalseLabel(THRESHOLDED_IMAGE);
        }

        @Override
        public void setSelected(boolean state) {
            super.setSelected(state);
            updateImageConfiguration(state);
        }

    }

    protected class TargetSwitch extends WheelSwitch {

        private static final long serialVersionUID = -2942548629490106644L;

        public TargetSwitch() {
            super();
        }

        @Override
        public void setValue(double val, boolean needFormat, boolean notifyMediators) {
            super.setValue(val, needFormat, notifyMediators);
            updateTargetDraw();
        }
    }

    protected class SensorTypeTarget extends ADataTarget {

        public SensorTypeTarget() {
            super();
        }

        @Override
        protected boolean isExpectedData(String data) {
            return IMAGE_SENSOR_TYPE.equals(data);
        }

        @Override
        protected void treatNoData() {
            disconnectSpecificProperties();
        }

        @Override
        protected void treatInvalidData() {
            disconnectSpecificProperties();
        }

        @Override
        protected void treatExpectedData() {
            connectSpecificProperties();
        }

    }

}
