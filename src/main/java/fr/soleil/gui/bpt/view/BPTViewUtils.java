package fr.soleil.gui.bpt.view;

import java.awt.Color;

import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

public class BPTViewUtils {

    public static final Color DARK_BLUE = new Color(0, 0, 180);
    public static final Color DARK_RED = new Color(100, 0, 0);

    private BPTViewUtils() {
        // hide constructor
    }

    public static TitledBorder createTitledBorder(String title, Color color) {
        TitledBorder border = new TitledBorder(new LineBorder(color), title);
        border.setTitleColor(color);
        return border;
    }

}
